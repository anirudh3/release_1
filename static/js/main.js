(function(M) {

  var Persona = function(logged_in_user, login_url, logout_url) {
    navigator.id.watch({
      loggedInUser: logged_in_user,
      //when an user logs in
      onlogin: function(assertion) {
        //verify assertion and login the user
        $.ajax({
          type: 'POST',
          url: login_url,
          data: {assertion: assertion},
          success: function(data) {
            console.log('successful login..', data);
            window.location.reload();
          },
          error: function() {
            navigator.id.logout();
          }
        });
      },
      onlogout: function() {
        $.ajax({
          type: 'POST',
          url: logout_url,
          success: function() {
            window.location.href = window.location.origin;
          },
          error: function() {
          }
        });
      }
    });

    return this;
  }

  Persona.prototype.attachLogin = function(login_btn) {
    // check if the login button exists
    if($(login_btn).length) {
      $(login_btn).click(function(e) {
        e.preventDefault();
        navigator.id.request();
      });
    }
  };

  Persona.prototype.attachLogout = function(logout_btn) {
    // check if the logout button exists
    if($(logout_btn).length) {
      $(logout_btn).click(function(e) {
        e.preventDefault();
        navigator.id.logout();
      });
    }
  };

  // post scores to the server
  // @score: and integer value of the score
  // @experiment: and integer value of the experiment number
  // @succ_cb: a callback function - which will get called when scores get
  //           posted
  M.postScore = function(score, experiment, level, succ_cb) {
    $.ajax({
      type: 'POST',
      url: M.scoreURL(),
      data: {score: score, experiment: experiment, level: level},
      success: function(resp) {
        if(succ_cb) {
          succ_cb(resp);
        }
      }
    });
  };

  M.saveRetentionTiming = function(data, succ_cb) {
    $.ajax({
      url: M.saveRetentionURL(),
      type: 'POST',
      data: data,
      success: function(resp) {
        if(succ_cb) {
          succ_cb(resp);
        }
      }
    });
  };

  M.collectRetentionAndSave = function(exp_num) {
    var start_time, end_time;
    start_time = Date.now();
    console.log('start time', start_time);

    window.addEventListener('beforeunload', function() {
      end_time = Date.now();
      console.log('end time', end_time);
      M.saveRetentionTiming({
        experiment: exp_num,
        start_time: start_time,
        end_time: end_time
      });
    });
  };

  var RenarrationView = Backbone.View.extend({
    el: $('body'),
    events: {
      'change #renarrate': 'renarrateClicked'
    },
    initialize: function() {
      this.available_renarrs = {};
      var selected = $('#renarrate option:selected').val();
      if(selected !== 'English') {
        this.renderPageWtLang(selected);
      }
    },
    renderPageWtLang: function(lang) {
      console.log('render page in', lang);
      if(lang == 'English') {
        //just reload the page..don't do anything
        window.location.reload();
      }
      else {
        this.renarrate(lang);
      }
    },
    renarrateClicked: function(e) {
      //console.log('renarr clicked');
      var lang = $(e.currentTarget).val();
      this.renderPageWtLang(lang);
    },
    renarrate: function(lang) {
      Renarrations.getAll({
        what: lang,
        where: encodeURIComponent(window.location.href),
        success: function(data) {
          console.log(data);
          _.each(data['r'], function(item) {
            _.each(item['narration'], function(narration) {
              Renarrations.add(narration);
            });
          });
          Renarrate(Renarrations);
        }
      });
    }
  });

  M.init = function() {
    this.persona = new Persona(M.loggedInUser(), M.loginURL(), M.logoutURL());
    //console.log(persona);
    var login_btn = $('a[href="#login"]')[0];
    var logout_btn = $('a[href="#logout"]')[0];
    //console.log(login_btn);
    this.persona.attachLogin(login_btn);
    this.persona.attachLogout(logout_btn);

    this.rennarr_view = new RenarrationView();
  };


})(window.ML);
