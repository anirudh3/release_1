var note_array=["g2","gs2","a2","as2","b2","c3","cs3","d3","ds3","e3","f3","fs3","g3","gs3","a3","as3","b3","c4","cs4","d4","ds4","e4","f4","fs4","g4"];
var instrument;
var tone1,tone2;
var played1,played2;
var score=0;
var total=0;
var num1,num2;

function init()
{
	var instruments=document.getElementById("ins");
	instrument=instruments.options[instruments.selectedIndex].innerHTML.toLowerCase();
	generate();
}
function generate()
{
	num1=Math.floor(Math.random()*25);
	num2=num1;
	played1=0;
	played2=0;
	while(num2==num1)
		num2=Math.floor(Math.random()*25);
	tone1="/static/stimuli/instruments/"+instrument+"/"+note_array[num1]+".mp3";
	tone2="/static/stimuli/instruments/"+instrument+"/"+note_array[num2]+".mp3";
}
function playsound1()
{
	var audioElement=document.createElement('audio');
	audioElement.setAttribute('src', tone1);
	audioElement.play();
	played1=1;
}
function playsound2()
{
	var audioElement=document.createElement('audio');
	audioElement.setAttribute('src', tone2);
	audioElement.play();
	played2=1;
}
function Tone1()
{
	if(played1==1 && played2==1)
	{
		if(num1>num2)
		{
			document.getElementById("div1-1").innerHTML="You are right!";
			total++;
			score++;
		}
		else
		{
			document.getElementById("div1-1").innerHTML="You are wrong.";
			total++;
		}
		document.getElementById("div1-2").innerHTML="Your score is "+score+" out of "+total;
		generate();
	}
}
function Tone2()
{
	if(played1==1 && played2==1)
	{
		if(num2>num1)
		{
			document.getElementById("div1-1").innerHTML="You are right!";
			total++;
			score++;
		}
		else
		{
			document.getElementById("div1-1").innerHTML="You are wrong.";
			total++;
		}
		document.getElementById("div1-2").innerHTML="Your score is "+score+" out of "+total;
		generate();
	}
}
