# -*- coding: utf-8 -*-

from flask import session
from flask.ext.sqlalchemy import SQLAlchemy
from datetime import datetime

from sqlalchemy.orm.exc import NoResultFound

db = SQLAlchemy()


class User(db.Model):
    """ docstring """

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64))
    email = db.Column(db.String(128), unique=True)
    total_score = db.Column(db.Integer, default=0)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_active = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, username, email):
        self.username = username
        self.email = email

    def update(self, **kwargs):
        if kwargs.get('username'):
            self.username = kwargs.get('username')
        if kwargs.get('total_score'):
            self.total_score = kwargs.get('total_score')

        if kwargs.get('last_active'):
            print 'Updated last_active timestamp %s for %s' %\
                (kwargs.get('last_active'), self)

            self.last_active = kwargs.get('last_active')

        self.persist()

    # persist current object in the database
    def persist(self):
        db.session.add(self)
        db.session.commit()

    # delete from database
    def remove(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def getCurrentUser():
        if 'email' in session:
            user = User.query.filter_by(email=session['email']).first()
            user.update(last_active=datetime.utcnow())
            return user
        return None

    @staticmethod
    def getByName(username):
        return User.query.filter_by(username=username).first()

    def save_score(self, score, exp_num, level):

        score = int(score)
        exp_num = int(exp_num)
        if level:
            level = int(level)

        experiment = Experiment.query.get(exp_num)
        if not experiment:
            raise NoResultFound

        new_score = Score(user=self, experiment=experiment, level=level,
                          score=score)
        new_score.persist()

        total_score = sum([row.score for row in
                           Score.query.filter_by(user=self)])

        #print total_score
        self.update(total_score=total_score)

    def save_retention(self, exp_num, start_time, end_time):

        experiment = Experiment.query.get(exp_num)
        if not experiment:
            raise NoResultFound

        start_time = int(start_time)
        end_time = int(end_time)
        print exp_num, start_time, end_time

        retention = RetentionTime(user=self, experiment=experiment,
                                  start_time=start_time, end_time=end_time)

        retention.persist()

    def to_dict(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'score': self.score,
            'created': self.created.isoformat(),
            #'created': self.created.strftime('%a, %d %b %Y, %I:%M %p UTC'),
            'last_active': self.last_active.isoformat()
            #'last_active': self.last_active.strftime('%a, %d %b %Y, %I:%M %p UTC')
        }

    def __repr__(self):
        return '<User:: %r %r>' % (self.username, self.email)


class Experiment(db.Model):
    __tablename__ = 'experiments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    filename = db.Column(db.String(128))

    def persist(self):
        db.session.add(self)
        db.session.commit()


class Score(db.Model):
    __tablename__ = 'scores'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User')

    exp_id = db.Column(db.Integer, db.ForeignKey('experiments.id'))
    experiment = db.relationship('Experiment')

    score = db.Column(db.Integer)
    level = db.Column(db.Integer)

    #time_taken = db.Column(db.Interval)

    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, **kwargs):
        self.user = kwargs.get('user')
        self.experiment = kwargs.get('experiment')
        self.level = kwargs.get('level')
        self.score = kwargs.get('score')

    def persist(self):
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return '<Score:: %r %r: level: %r : %r>' % (self.user, self.experiment,
                                                    self.level, self.score)


class RetentionTime(db.Model):

    __tablename__ = 'retention_times'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User')

    exp_id = db.Column(db.Integer, db.ForeignKey('experiments.id'))
    experiment = db.relationship('Experiment')

    #level = db.Column(db.Integer)

    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)

    # this is in seconds
    time_spent = db.Column(db.Integer)

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        print 'initing after super::retentiontime'
        # convert to datetime objects from timestamps in Javascript
        self.start_time = datetime.utcfromtimestamp(self.start_time / 1000.0)
        self.end_time = datetime.utcfromtimestamp(self.end_time / 1000.0)
        print self.start_time
        print type(self.start_time)
        time_spent = self.end_time - self.start_time
        self.time_spent = time_spent.total_seconds()

    def persist(self):
        db.session.add(self)
        db.session.commit()
