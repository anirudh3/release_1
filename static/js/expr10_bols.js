(function(window) {
var path = '/static/bols';
var bol_elem = document.createElement('audio');
var bol_dha = ["dhe","dhin1","dhir1","dhun1"];
var bol_ge  = ["ge1","ge2","ge3","ge4","ge5","ge6","ge7","ge8","ge9","ghe1"];
var bol_ka  = ["ka1","ka2","ka3","kat1"];
var bol_na  = ["na1","na2","na3","na4","na5"];
var bol_re  = ["re1","re2"];
var bol_ta  = ["ta1","ta3","ta4","ta5","taka1","te1","tin1","tita1","tuh1","tuh2","tuh3","tun1","tun2","tun3","tun4"];

function playbol_dha() {
var note =  bol_dha[Math.floor(Math.random() * bol_dha.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
}

function playbol_ge() {
var note =  bol_ge[Math.floor(Math.random() * bol_ge.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
}

function playbol_ka() {
var note =  bol_ka[Math.floor(Math.random() * bol_ka.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
}

function playbol_na() {
var note =  bol_na[Math.floor(Math.random() * bol_na.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
}

function playbol_re() {
var note =  bol_re[Math.floor(Math.random() * bol_re.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
}

function playbol_ta() {
var note =  bol_ta[Math.floor(Math.random() * bol_ta.length)];
var currentnote_path = path + '/'  + note + '.wav';
bol_elem.setAttribute('src', currentnote_path);
  bol_elem.play();
}

window.playbol_dha = playbol_dha;
window.playbol_ge = playbol_ge;
window.playbol_ka = playbol_ka;
window.playbol_na = playbol_na;
window.playbol_re = playbol_re;
window.playbol_ta = playbol_ta;

})(window);
