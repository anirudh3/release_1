var nodes;
var edges;
var score=0;
var total=0;
var pos;
var ans;
var dic={"raga":[],"thaat":[],"jati":[],"time":[],"svar":[],"pakad":[],"aaroh":[],"avroh":[]};
$(document).ready(function()
		{
		$.ajax(
			{
type:"GET",
url:"/static/data/nodes1.csv",
dataType: "text",
success: function(data) { parsenodes(data); }
});
		$.ajax(
			{
type:"GET",
url: "/static/data/edges1.csv",
dataType: "text",
success: function(data) { parseedges(data); }
});
		});
function parsenodes(data)
{
	console.log("opening file");
	nodes=$.csv.toObjects(data);
	console.log("file parsed");
}
function parseedges(data)
{
	console.log("opening file");
	edges=$.csv.toObjects(data);
	console.log("file parsed");
	distribute();
}
function distribute()
{
	for(i=1;i<386;i++)
	{
		x=nodes[i-1]["Type"].replace(/'/g, "");
		dic[x].push(nodes[i-1].Label);
	}
	dic['aaroh']=new Array(163);
	dic['avroh']=new Array(163);
	for(i=0;i<163;i++)
	{
		dic['aaroh'][i]=' ';
		dic['avroh'][i]=' ';
	}
	for(i=979;i<3750;i++)
	{
		dic['aaroh'][edges[i-1].Source-1]=dic['aaroh'][edges[i-1].Source-1]+' '+nodes[edges[i-1].Target-1].Label.replace(/'/g, "");
	}
	console.log(edges.length);
	for(i=3750;i<7004;i++)
	{
		if(isNaN(edges[i-1].Target-1))
			console.log(i);
		dic['avroh'][edges[i-1].Source-1]=dic['avroh'][edges[i-1].Source-1]+' '+nodes[edges[i-1].Target-1].Label.replace(/'/g, "");
	}
	generate();
}
function submit()
{
	if(!(document.getElementById("op1").checked==true || document.getElementById("op2").checked==true || document.getElementById("op3").checked==true || document.getElementById("op4").checked==true))
		return;
	if(document.getElementById("op1").checked==true && pos==0)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else if(document.getElementById("op2").checked==true && pos==1)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else if(document.getElementById("op3").checked==true && pos==2)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else if(document.getElementById("op4").checked==true && pos==3)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else
	{
		document.getElementById("rightorwrong").innerHTML="You are wrong.";
	}
	total++;
	document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	document.getElementById("submit").disabled=true;
}
function showanswer()
{
	if(!document.getElementById("submit").disabled)
	{
		console.log("submit not disabled");
		total++;
		document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	}
	document.getElementById("answer").innerHTML="The correct answer is: "+ans;
	document.getElementById("show").disabled=true;
	document.getElementById("submit").disabled=true;
}
function next()
{
	if(document.getElementById("show").disabled==false && document.getElementById("submit").disabled==false)
	{
		total++;
		document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	}
	document.getElementById("show").disabled=false;
	document.getElementById("submit").disabled=false;
	document.getElementById("answer").innerHTML="";
	document.getElementById("op1").checked=false;
	document.getElementById("op2").checked=false;
	document.getElementById("op3").checked=false;
	document.getElementById("op4").checked=false;
	document.getElementById("rightorwrong").innerHTML="";
	generate();
}
function generate()
{
	var ri=Math.floor(Math.random()*6);
	if(ri==0)
	{
		var nno=Math.floor(Math.random()*(dic['aaroh'].length-1));
		document.getElementById("ques").innerHTML='What is the aaroh of '+nodes[nno].Label.replace(/'/g,"")+'?';
		anstype='aaroh';
		ans=dic['aaroh'][nno];
	}
	else if(ri==1)
	{
		var nno=Math.floor(Math.random()*(dic['avroh'].length-1));
		document.getElementById("ques").innerHTML='What is the avroh of '+nodes[nno].Label.replace(/'/g,"")+'?';
		anstype='avroh';
		ans=dic['avroh'][nno];
	}
	else
	{
		var ran=Math.floor(Math.random()*(980-1));
		var source=nodes[edges[ran].Source-1].Label;
		var target=nodes[edges[ran].Target-1].Label;
		var label=edges[ran].Label;
		ans=target;
		var anstype=nodes[edges[ran].Target-1].Type.replace(/'/g, "");
		document.getElementById("ques").innerHTML='What is the '+label.replace(/'/g,"")+' of '+source.replace(/'/g,"")+'?';
	}
	var others=new Array(3);
	var index=0;
	var rn,i;
	while(index!=3)
	{
		flag=0;
		while(flag==0)
		{
			rn=Math.floor(Math.random()*(dic[anstype].length-1));
			if(dic[anstype][rn]==ans)
				continue;
			for(i=0;i<index;i++)
				if(others[i]==dic[anstype][rn])
				{
					flag=1;
					break;
				}
			if(flag==0)
				break;
			if(flag==1)
				flag=0;
		}
		others[index]=dic[anstype][rn];
		index++;
	}
	ans=ans.replace(/'/g, "");
	for(i=0;i<3;i++)
		others[i]=others[i].replace(/'/g, "");
	pos=Math.floor(Math.random()*3);
	console.log(others);
	console.log('ans:'+ans);
	if(pos==0)
	{
		document.getElementById("op1").nextSibling.data=ans;
		document.getElementById("op2").nextSibling.data=others[0];
		document.getElementById("op3").nextSibling.data=others[1];
		document.getElementById("op4").nextSibling.data=others[2];
	}
	else if(pos==1)
	{
		document.getElementById("op1").nextSibling.data=others[0];
		document.getElementById("op2").nextSibling.data=ans;
		document.getElementById("op3").nextSibling.data=others[1];
		document.getElementById("op4").nextSibling.data=others[2];
	}
	else if(pos==2)
	{
		document.getElementById("op1").nextSibling.data=others[0];
		document.getElementById("op2").nextSibling.data=others[1];
		document.getElementById("op3").nextSibling.data=ans;
		document.getElementById("op4").nextSibling.data=others[2];
	}
	else
	{
		document.getElementById("op1").nextSibling.data=others[0];
		document.getElementById("op2").nextSibling.data=others[1];
		document.getElementById("op3").nextSibling.data=others[2];
		document.getElementById("op4").nextSibling.data=ans;
	}
}
