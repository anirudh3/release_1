var nodes;
var edges;
var score=0;
var total=0;
var pos;
var ans;
var dic={"taal":[],"bol":[],"matra":[],"jati":[],"taalbol":[],"vibhaag":[]};
var nummap={
	"1":"first",
	"2":"second",
	"3":"third",
	"4":"fourth",
	"5":"fifth",
	"6":"sixth",
	"7":"seventh",
	"8":"eighth",
	"9":"ninth",
	"10":"tenth",
	"11":"eleventh",
	"12":"twelfth",
	"13":"thirteenth",
	"14":"fourteenth",
	"15":"fifteenth",
	"16":"sixteenth",
	"17":"seventeenth",
	"18":"eighteenth",
	"19":"nineteenth",
	"20":"twentieth",
	"21":"twenty-first",
	"22":"twenty-second"};

function compare(arr1,arr2) 
{
	console.log("in compare");
	if(arr1.length!=arr2.length) 
		return false;
	if(arr1.length==0)
		return true;
	for(var i=0;i<arr1.length;i++) 
	{
		if (arr1[i]!==arr2[i]) 
			return false;
	}
	return true;
}
$(document).ready(function()
		{
		$.ajax(
			{
type:"GET",
url:"/static/data/nodes1taal.csv",
dataType: "text",
success: function(data) { parsenodes(data); }
});
		$.ajax(
			{
type:"GET",
url: "/static/data/edges1taal.csv",
dataType: "text",
success: function(data) { parseedges(data); }
});
		});
function parsenodes(data)
{
	//console.log("opening file");
	nodes=$.csv.toObjects(data);
	//console.log("file parsed");
}
function parseedges(data)
{
	//console.log("opening file");
	edges=$.csv.toObjects(data);
	//console.log("file parsed");
	distribute();
}
function distribute()
{
	for(i=1;i<97;i++)
	{
		x=nodes[i-1]["Type"].replace(/'/g, "");
		//console.log(x);
		dic[x].push(nodes[i-1].Label);
	}
	dic['taalbol']=new Array(24);
	dic['vibhaag']=new Array(24);
	dic['taali']=new Array(24);
	dic['khali']=new Array(24);
	for(i=0;i<24;i++)
	{
		dic['taalbol'][i]=new Array();
		dic['vibhaag'][i]=new Array();
		dic['taali'][i]=new Array();
		dic['khali'][i]=new Array();
	}
	for(i=68;i<357;i++)
		dic['taalbol'][edges[i].Source-1].push(nodes[edges[i].Target-1].Label.replace(/'/g,""));
	for(i=357;i<453;i++)
		dic['vibhaag'][edges[i].Source-1].push(nodes[edges[i].Target-1].Label.replace(/'/g,""));
	for(i=453;i<499;i++)
		dic['taali'][edges[i].Source-1].push(nodes[edges[i].Target-1].Label.replace(/'/g,""));
	for(i=499;i<522;i++)
		dic['khali'][edges[i].Source-1].push(nodes[edges[i].Target-1].Label.replace(/'/g,""));
	generate();
}
function submit()
{
	if(!(document.getElementById("op1").checked==true || document.getElementById("op2").checked==true || document.getElementById("op3").checked==true || document.getElementById("op4").checked==true))
		return;
	if(document.getElementById("op1").checked==true && pos==0)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else if(document.getElementById("op2").checked==true && pos==1)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else if(document.getElementById("op3").checked==true && pos==2)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else if(document.getElementById("op4").checked==true && pos==3)
	{
		document.getElementById("rightorwrong").innerHTML="You are right!";
		score++;
	}
	else
	{
		document.getElementById("rightorwrong").innerHTML="You are wrong.";
	}
	total++;
	document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	document.getElementById("submit").disabled=true;
}
function showanswer()
{
	if(!document.getElementById("submit").disabled)
	{
		//console.log("submit not disabled");
		total++;
		document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	}
	document.getElementById("answer").innerHTML="The correct answer is: "+ans;
	document.getElementById("show").disabled=true;
	document.getElementById("submit").disabled=true;
}
function next()
{
	if(document.getElementById("show").disabled==false && document.getElementById("submit").disabled==false)
	{
		total++;
		document.getElementById("score").innerHTML="Score: "+score+"/"+total;
	}
	document.getElementById("show").disabled=false;
	document.getElementById("submit").disabled=false;
	document.getElementById("answer").innerHTML="";
	document.getElementById("op1").checked=false;
	document.getElementById("op2").checked=false;
	document.getElementById("op3").checked=false;
	document.getElementById("op4").checked=false;
	document.getElementById("rightorwrong").innerHTML="";
	generate();
}
function generate()
{
	var ri=Math.floor(Math.random()*6);
	//console.log("next question");
	var others=new Array(3);
	if(ri==0 || ri==1)
	{
		var ran=Math.floor(Math.random()*68);
		var source=nodes[edges[ran].Source-1].Label;
		var target=nodes[edges[ran].Target-1].Label;
		var label=edges[ran].Label;
		ans=target;
		var anstype=nodes[edges[ran].Target-1].Type.replace(/'/g, "");
		document.getElementById("ques").innerHTML='What is the '+label.replace(/'/g,"")+' of '+source.replace(/'/g,"")+'?';
	}
	else if(ri==2)
	{
		var ran=Math.floor(Math.random()*24);
		var ran2=Math.floor(Math.random()*dic['taalbol'][ran].length);
		document.getElementById("ques").innerHTML='What is the '+nummap[ran2+1]+' bol of '+dic['taal'][ran].replace(/'/g,"")+'?';
		ans=dic['taalbol'][ran][ran2];
		anstype='bol';
	}
	else if(ri==3)
	{
		var ran=Math.floor(Math.random()*24);
		document.getElementById("ques").innerHTML='At what matra/s does '+dic['taal'][ran].replace(/'/g,"")+' have a vibhaag?';
		anstype='vibhaag';
		ans=dic['vibhaag'][ran];
	}
	else if(ri==4)
	{
		var ran=Math.floor(Math.random()*24);
		document.getElementById("ques").innerHTML='At what matra/s does '+dic['taal'][ran].replace(/'/g,"")+' have a taali?';
		ans=dic['taali'][ran];
		anstype='taali';
	}
	else if(ri==5)
	{
		var ran=Math.floor(Math.random()*24);
		document.getElementById("ques").innerHTML='At what matra/s does '+dic['taal'][ran].replace(/'/g,"")+' have a khali?';
		ans=dic['khali'][ran];
		anstype='khali';
	}
	//console.log(ans);
	//console.log(anstype);
	var others=new Array(3);
	var index=0;
	var rn,i;
	if(anstype==='jati')
	{
		console.log("jati");
		for(i=0;i<4;i++)
			if(dic['jati'][i]!=ans)
			{
				others[index]=dic['jati'][i];
				index++;
			}
	}
	else
	{
		while(index!=3)
		{
			flag=0;
			while(flag==0)
			{
				rn=Math.floor(Math.random()*(dic[anstype].length-1));
				if((Array.isArray(ans) && (compare(ans,dic[anstype][rn]))) || dic[anstype][rn]==ans)
					continue;
				for(i=0;i<index;i++)
					if((Array.isArray(others[i]) && (compare(others[i],dic[anstype][rn]))) || others[i]==dic[anstype][rn])
					{
						flag=1;
						break;
					}
				if(flag==0)
					break;
				if(flag==1)
					flag=0;
			}
			others[index]=dic[anstype][rn];
			console.log(others[index]);
			index++;
		}
	}
	if(Array.isArray(ans) && ans.length==0)
		ans='none';
	for(i=0;i<3;i++)
		if(Array.isArray(others[i]) && others[i].length==0)
			others[i]='none';
	//console.log(others);
	if(!Array.isArray(ans))
		ans=ans.replace(/'/g, "");
	for(i=0;i<3;i++)
		if(!Array.isArray(others[i]))
			others[i]=others[i].replace(/'/g, "");
	pos=Math.floor(Math.random()*3);
	//console.log(others);
	//console.log('ans:'+ans);
	if(pos==0)
	{
		document.getElementById("op1").nextSibling.data=ans;
		document.getElementById("op2").nextSibling.data=others[0];
		document.getElementById("op3").nextSibling.data=others[1];
		document.getElementById("op4").nextSibling.data=others[2];
	}
	else if(pos==1)
	{
		document.getElementById("op1").nextSibling.data=others[0];
		document.getElementById("op2").nextSibling.data=ans;
		document.getElementById("op3").nextSibling.data=others[1];
		document.getElementById("op4").nextSibling.data=others[2];
	}
	else if(pos==2)
	{
		document.getElementById("op1").nextSibling.data=others[0];
		document.getElementById("op2").nextSibling.data=others[1];
		document.getElementById("op3").nextSibling.data=ans;
		document.getElementById("op4").nextSibling.data=others[2];
	}
	else
	{
		document.getElementById("op1").nextSibling.data=others[0];
		document.getElementById("op2").nextSibling.data=others[1];
		document.getElementById("op3").nextSibling.data=others[2];
		document.getElementById("op4").nextSibling.data=ans;
	}
}
