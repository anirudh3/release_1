(function(window) {

// global vars..
var current_notes = [],
    tone_elems = [],
    drone_elem,
    path = '/static/stimuli/instruments',
    note_array = ["c3","cs3","d3","ds3","e3","f3","fs3","g3","gs3","a3","as3","b3","c4"],
    note_names = ["Sa","re","Re","ga","Ga","Ma","Ma#","Pa","dha","Dha","ni","Ni","Sa1"],
    levels = {
      "1": {rows: 3, notes: [0, 2, 4, 7, 9]},
      "2": {rows: 3, notes: [0, 2, 4, 5, 7, 9, 11]},
      "3": {rows: 4, notes: [0, 2, 4, 5, 7, 9, 11]},
      "4": {rows: 3, notes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]},
      "5": {rows: 4, notes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}
    },
    playing = false,
    correct = 0,
    incorrect = 0,
    current=0,
    answer,
    tonicplaying=0,
    level=0;


// initialize stuff..
function init() {
  // append audio elements
  tone_elems = [
    document.createElement('audio'),
    document.createElement('audio'),
    document.createElement('audio'),
    document.createElement('audio')
  ];
  drone_elem = document.createElement('audio');
  drone_elem.setAttribute('src', '/static/drones/3.mp3');

  $(".flip").click(function(e) {
    e.preventDefault();
    var target = $(this).attr("href");
    $(target).slideToggle("fast");
    console.log(target);
    $(".panel").not(target).hide();
    //var level=$('#level-selector li.active').val();
    level = target.split('panel')[1];
    console.log(level);
    current=0;
    if(level==1)
    {
    	console.log("in level 1");
    	Mousetrap.reset();
	var buttons=document.getElementsByName("l1");
	for(i=0;i<3;i++)
	Mousetrap.bind('tab',function(e) { e.preventDefault(); nextbutton(1); });
	Mousetrap.bind('1',function() { buttons[current].innerHTML="Sa"; nextbutton(1); });	
	Mousetrap.bind('2',function() { buttons[current].innerHTML="Re"; nextbutton(1); });
	Mousetrap.bind('3',function() { buttons[current].innerHTML="Ga"; nextbutton(1); });
	Mousetrap.bind('5',function() { buttons[current].innerHTML="Pa"; nextbutton(1); });
	Mousetrap.bind('6',function() { buttons[current].innerHTML="Dha"; nextbutton(1); });
	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });

    }
    else if(level==2)
    {
    	Mousetrap.reset();
	var buttons=document.getElementsByName("l2");
	Mousetrap.bind('tab',function(e) { e.preventDefault(); nextbutton(2); });
	Mousetrap.bind('1',function() { buttons[current].innerHTML="Sa"; nextbutton(2); });	
	Mousetrap.bind('2',function() { buttons[current].innerHTML="Re"; nextbutton(2); });
	Mousetrap.bind('3',function() { buttons[current].innerHTML="Ga"; nextbutton(2); });
	Mousetrap.bind('4',function() { buttons[current].innerHTML="Ma"; nextbutton(2); });
	Mousetrap.bind('5',function() { buttons[current].innerHTML="Pa"; nextbutton(2); });
	Mousetrap.bind('6',function() { buttons[current].innerHTML="Dha"; nextbutton(2); });
	Mousetrap.bind('7',function() { buttons[current].innerHTML="Ni"; nextbutton(2); });
	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });

    }
    else if(level==3)
    {
    	Mousetrap.reset();
	var buttons=document.getElementsByName("l3");
	Mousetrap.bind('tab',function(e) { e.preventDefault(); nextbutton(3); });
	Mousetrap.bind('1',function() { buttons[current].innerHTML="Sa"; nextbutton(3); });	
	Mousetrap.bind('2',function() { buttons[current].innerHTML="Re"; nextbutton(3); });
	Mousetrap.bind('3',function() { buttons[current].innerHTML="Ga"; nextbutton(3); });
	Mousetrap.bind('4',function() { buttons[current].innerHTML="Ma"; nextbutton(3); });
	Mousetrap.bind('5',function() { buttons[current].innerHTML="Pa"; nextbutton(3); });
	Mousetrap.bind('6',function() { buttons[current].innerHTML="Dha"; nextbutton(3); });
	Mousetrap.bind('7',function() { buttons[current].innerHTML="Ni"; nextbutton(3); });
	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });

    }
    else if(level==4)
    {
    	Mousetrap.reset();
	var buttons=document.getElementsByName("l4");
	Mousetrap.bind('tab',function(e) { e.preventDefault(); nextbutton(4); });
	Mousetrap.bind('1',function() { buttons[current].innerHTML="Sa"; nextbutton(4); });
	Mousetrap.bind('shift+2',function() { buttons[current].innerHTML="re"; nextbutton(4); });
	Mousetrap.bind('2',function() { buttons[current].innerHTML="Re"; nextbutton(4); });
	Mousetrap.bind('shift+3',function() { buttons[current].innerHTML="ga"; nextbutton(4); });
	Mousetrap.bind('3',function() { buttons[current].innerHTML="Ga"; nextbutton(4); });
	Mousetrap.bind('4',function() { buttons[current].innerHTML="Ma"; nextbutton(4); });
	Mousetrap.bind('shift+4',function() { buttons[current].innerHTML="Ma#"; nextbutton(4); });
	Mousetrap.bind('5',function() { buttons[current].innerHTML="Pa"; nextbutton(4); });
	Mousetrap.bind('shift+6',function() { buttons[current].innerHTML="dha"; nextbutton(4); });
	Mousetrap.bind('6',function() { buttons[current].innerHTML="Dha"; nextbutton(4); });
	Mousetrap.bind('shift+7',function() { buttons[current].innerHTML="ni"; nextbutton(4); });
	Mousetrap.bind('7',function() { buttons[current].innerHTML="Ni"; nextbutton(4); });
	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });

    }
    else if(level==5)
    {
    	Mousetrap.reset();
	var buttons=document.getElementsByName("l5");
	Mousetrap.bind('tab',function(e) { e.preventDefault(); nextbutton(5); });
	Mousetrap.bind('1',function() { buttons[current].innerHTML="Sa"; nextbutton(5); });
	Mousetrap.bind('shift+2',function() { buttons[current].innerHTML="re"; nextbutton(5); });
	Mousetrap.bind('2',function() { buttons[current].innerHTML="Re"; nextbutton(5); });
	Mousetrap.bind('shift+3',function() { buttons[current].innerHTML="ga"; nextbutton(5); });
	Mousetrap.bind('3',function() { buttons[current].innerHTML="Ga"; nextbutton(5); });
	Mousetrap.bind('4',function() { buttons[current].innerHTML="Ma"; nextbutton(5); });
	Mousetrap.bind('shift+4',function() { buttons[current].innerHTML="Ma#"; nextbutton(5); });
	Mousetrap.bind('5',function() { buttons[current].innerHTML="Pa"; nextbutton(5); });
	Mousetrap.bind('shift+6',function() { buttons[current].innerHTML="dha"; nextbutton(5); });
	Mousetrap.bind('6',function() { buttons[current].innerHTML="Dha"; nextbutton(5); });
	Mousetrap.bind('shift+7',function() { buttons[current].innerHTML="ni"; nextbutton(5); });
	Mousetrap.bind('7',function() { buttons[current].innerHTML="Ni"; nextbutton(5); });
	Mousetrap.bind('shift+space', function(e) { e.preventDefault(); if(tonicplaying==true) { stop_tonic(); } else { play_tonic(); } });
	Mousetrap.bind('space', function(e) { e.preventDefault(); playtone_clicked(); });
	Mousetrap.bind('enter', function(e) { e.preventDefault(); submit_clicked(); });

    }

    $(target).slideDown("fast");
    return false;
  });

  ML.collectRetentionAndSave(6);
}

function fillbox(level,notename)
{
	var buttons=document.getElementsByName("l"+level);
	buttons[current].innerHTML=notename;
	nextbutton(level);
}

function nextbutton(level)
{
	if(current==levels[level]['rows']-1)
		current=0;
	else
		current++;
	console.log('current',current);
}

// handler when play tone btn clicked
function playtone_clicked() {
	document.getElementById("ans").innerHTML='';
   var instrument= $('#instrument option:selected').val();
   console.log(level, instrument);
   play_tone(level, instrument);
}

// generic function to play a tone based on level and instrument
function play_tone(level, instrument) {
  //var currentnote_path;
  // if user has not submitted, play the current note
  if(playing === true) {
    var i = 0;
    (function iter() {
      //console.log(current_notes[i]);
      play(i, instrument);

      if(++i < current_notes.length) {
        setTimeout(iter, 2000);
      }
    })();
  }
  // else generate a new random note; and that becomes the current note
  else {
    current_notes = get_random_notes(level);
    playing = true;
    var i = 0;
    (function iter() {
      //console.log(current_notes[i]);
      play(i, instrument);

      if(++i < current_notes.length) {
        setTimeout(iter, 2000);
      }
    })();
  }
}

function play(i, instrument) {
    //console.log('iter', i);
    var currentnote_path = path + '/' + instrument + '/' +
      current_notes[i] + '.mp3';

    //console.log('playing', currentnote_path, i, tone_elems[i]);
    tone_elems[i].setAttribute('src', currentnote_path);
    tone_elems[i].play();
}

// return a random note based on the level given..
function get_random_notes(l) {
  //console.log('got level', level);
  //var level='"'+l+'"';
  console.log(level);
  console.log(levels[level]);
  var level_array = levels[level]["notes"];
  console.log('level array', level_array);
  var notes = [];
  answer=[];
  for(var i = 0; i < levels[level]['rows']; i++) {
    var noteno = level_array[Math.floor(Math.random() * level_array.length)];
    //console.log('note no', noteno);
    notes.push(note_array[noteno]);
    answer.push(note_names[noteno]);
  }
  //return note_array[noteno];
  console.log(answer);
  return notes;
}

function show_answer()
{
	console.log(answer);
	console.log("in show ans");
	document.getElementById("ans").innerHTML=answer;
	playing=false;
	for(i=0;i<levels[level]["rows"];i++)
	{
		buttons[i].innerHTML="";
	}
	if(tonicplaying==true)
	{
		stop_tonic();
		tonicplaying=false;
	}
	incorrect++;
	var total=correct+incorrect;
	document.getElementById("score").innerHTML="Score: "+correct+"/"+total;


}

function submit_clicked() 
{
	var i;
	wrong=0;
	console.log('playing',playing);
	if(playing===false)
		return;
	var buttons=document.getElementsByName("l"+level);
	for(i=0;i<levels[level]["rows"];i++)
	{
		if(buttons[i].innerHTML.localeCompare(answer[i])!=0)
		{
			wrong=1;
			console.log("wrong",i);
			break;
		}
	}
	if(wrong==0)
	{
		document.getElementById("answer").innerHTML="You are right!";
		correct++;
		var total=correct+incorrect;
		document.getElementById("score").innerHTML="Score: "+correct+"/"+total;
	}
	else
	{
		document.getElementById("answer").innerHTML="You are wrong.";
		incorrect++;
		var total=correct+incorrect;
		document.getElementById("score").innerHTML="Score: "+correct+"/"+total;
		return;
	}
	playing=false;
	for(i=0;i<levels[level]["rows"];i++)
	{
		buttons[i].innerHTML="";
	}
	if(tonicplaying==true)
	{
		stop_tonic();
		tonicplaying=false;
	}
}

function setcurrent(c)
{
	current=c;
}

function play_tonic() {
  drone_elem.play();
  tonicplaying=true;
}

function stop_tonic() {
  drone_elem.pause();
  tonicplaying=false;
}

// expose public methods..
window.init = init;
window.playtone_clicked = playtone_clicked;
window.submit_clicked = submit_clicked;
window.play_tonic = play_tonic;
window.stop_tonic = stop_tonic;
window.fillbox=fillbox;
window.setcurrent=setcurrent;
window.show_answer=show_answer;
})(window);
